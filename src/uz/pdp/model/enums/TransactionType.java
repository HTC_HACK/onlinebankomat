package uz.pdp.model.enums;

public enum TransactionType {
    CART_TO_CARD,
    FILL_BALANCE,
    WITHDRAW_MONEY,
    PAY_INDUSTRY
}

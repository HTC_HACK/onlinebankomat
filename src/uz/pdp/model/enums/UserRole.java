package uz.pdp.model.enums;

public enum UserRole {
    ADMIN,
    USER
}

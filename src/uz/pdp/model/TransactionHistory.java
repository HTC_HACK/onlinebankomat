package uz.pdp.model;

import uz.pdp.model.enums.TransactionType;

public class TransactionHistory {

    private Integer id;
    private Bankomat bankomat;
    private BankCard bankCard;
    private String where;

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public TransactionHistory(Integer id, Bankomat bankomat, BankCard bankCard, String where, TransactionType type, double amount) {
        this.id = id;
        this.bankomat = bankomat;
        this.bankCard = bankCard;
        this.where = where;
        this.type = type;
        this.amount = amount;
    }

    private TransactionType type;
    private double amount;

    public TransactionHistory() {
    }

    public TransactionHistory(Integer id, Bankomat bankomat, BankCard bankCard, TransactionType type, double amount) {
        this.id = id;
        this.bankomat = bankomat;
        this.bankCard = bankCard;
        this.type = type;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Bankomat getBankomat() {
        return bankomat;
    }

    public void setBankomat(Bankomat bankomat) {
        this.bankomat = bankomat;
    }

    public BankCard getBankCard() {
        return bankCard;
    }

    public void setBankCard(BankCard bankCard) {
        this.bankCard = bankCard;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "TransactionHistory{" +
                "id=" + id +
                ", bankomat=" + bankomat +
                ", bankCard=" + bankCard +
                ", type=" + type +
                ", amount=" + amount +
                '}';
    }
}

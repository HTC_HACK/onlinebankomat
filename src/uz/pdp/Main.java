package uz.pdp;


import uz.pdp.services.UserServiceImpl;

import static uz.pdp.DB.*;
import static uz.pdp.model.messages.MessageHelper.*;

public class Main {

    static UserServiceImpl userService = new UserServiceImpl();

    public static void main(String[] args) {

        boolean active = true;
        while (active) {
            System.out.println("1=>Login. 2=>Register. 3=>Exit");
            int option = scannerInt.nextInt();

            switch (option) {
                case 1:
                    userService.login();
                    break;
                case 2:
                    userService.register();
                    break;
                case 3:
                    active = false;
                    break;
                default:
                    System.out.println(WRONG_OPTION);
                    break;
            }
        }

    }
}

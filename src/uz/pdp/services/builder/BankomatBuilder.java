package uz.pdp.services.builder;

import uz.pdp.model.Bankomat;

public interface BankomatBuilder {

    Bankomat create(Integer id, String name, double commissionFee);
}

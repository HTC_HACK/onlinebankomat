package uz.pdp.services.builder;

import uz.pdp.model.User;
import uz.pdp.model.enums.UserRole;

@FunctionalInterface
public interface UserBuilder {

    User create(Integer id, String username, String password, UserRole role);

}

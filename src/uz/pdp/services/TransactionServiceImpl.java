package uz.pdp.services;

import uz.pdp.model.BankCard;
import uz.pdp.model.Bankomat;
import uz.pdp.model.TransactionHistory;
import uz.pdp.model.User;
import uz.pdp.model.enums.TransactionType;
import uz.pdp.services.action.ExistAction;
import uz.pdp.services.interfaceSer.TransactionService;

import static uz.pdp.DB.*;
import static uz.pdp.model.messages.MessageHelper.NOT_FOUND;
import static uz.pdp.model.messages.MessageHelper.WRONG_OPTION;

public class TransactionServiceImpl implements TransactionService {

    static ExistAction existAction = new ExistAction();

    @Override
    public void transactionList(User user) {
        switch (user.getRole()) {
            case ADMIN: {
                transactionHistoryList.stream()
                        .forEach(transactionHistory ->
                        {
                            System.out.println("Type     : " + transactionHistory.getType());
                            System.out.println("Card     : " + transactionHistory.getBankCard().getCardNumber());
                            System.out.println("Bankomat : " + transactionHistory.getBankomat().getName());
                            System.out.println("Amount   : " + transactionHistory.getAmount());
                            System.out.println("Where    : " + transactionHistory.getWhere());
                            System.out.println("Fee      : " + transactionHistory.getBankomat().getCommissionFee());
                            System.out.println("Total    : " +
                                    (transactionHistory.getAmount() +
                                            (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100)));

                            System.out.println("--------------------------------------------");
                        });
                break;
            }
            case USER: {
                transactionHistoryList.stream()
                        .filter(transactionHistory -> transactionHistory.getBankCard().getUser().equals(user))
                        .forEach(transactionHistory ->
                        {
                            System.out.println("Type     : " + transactionHistory.getType());
                            System.out.println("Card     : " + transactionHistory.getBankCard().getCardNumber());
                            System.out.println("Bankomat : " + transactionHistory.getBankomat().getName());
                            System.out.println("Amount   : " + transactionHistory.getAmount());
                            System.out.println("Where    : " + transactionHistory.getWhere());
                            System.out.println("Fee      : " + transactionHistory.getBankomat().getCommissionFee());
                            System.out.println("Total    : " +
                                    (transactionHistory.getAmount() +
                                            (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100)));
                            System.out.println("--------------------------------------------");

                        });
                break;
            }
        }
    }

    @Override
    public void transactionListByCard(User user) {
        transactionList(user);
        System.out.println("Enter cardnumber too see total transaction");
        long number = scannerLong.nextLong();
        BankCard card = existAction.findBankCardNum(number);
        if (card != null) {
            switch (user.getRole()) {
                case ADMIN: {
                    double total = 0;
                    for (TransactionHistory transactionHistory : transactionHistoryList) {
                        if (transactionHistory.getBankCard().equals(card)) {
                            System.out.println("Type     : " + transactionHistory.getType());
                            System.out.println("Card     : " + transactionHistory.getBankCard().getCardNumber());
                            System.out.println("Bankomat : " + transactionHistory.getBankomat().getName());
                            System.out.println("Amount   : " + transactionHistory.getAmount());
                            System.out.println("Where    : " + transactionHistory.getWhere());
                            System.out.println("Fee      : " + transactionHistory.getBankomat().getCommissionFee());
                            System.out.println("Total    : " +
                                    (transactionHistory.getAmount() +
                                            (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100)));
                            total += (transactionHistory.getAmount() +
                                    (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100));
                            System.out.println("--------------------------------------------");
                        }

                    }
                    System.out.println("Total : " + total);
                    System.out.println("--------------------------------------------");
                }
                case USER: {
                    double total = 0;
                    for (TransactionHistory transactionHistory : transactionHistoryList) {
                        if (transactionHistory.getBankCard().equals(card) && transactionHistory.getBankCard().getUser().equals(user)) {
                            System.out.println("Type     : " + transactionHistory.getType());
                            System.out.println("Card     : " + transactionHistory.getBankCard().getCardNumber());
                            System.out.println("Bankomat : " + transactionHistory.getBankomat().getName());
                            System.out.println("Amount   : " + transactionHistory.getAmount());
                            System.out.println("Where    : " + transactionHistory.getWhere());
                            System.out.println("Fee      : " + transactionHistory.getBankomat().getCommissionFee());
                            System.out.println("Total    : " +
                                    (transactionHistory.getAmount() +
                                            (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100)));
                            total += (transactionHistory.getAmount() +
                                    (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100));
                            System.out.println("--------------------------------------------");
                        }


                    }
                    System.out.println("Total : " + total);
                    System.out.println("--------------------------------------------");
                }
            }


        } else {
            System.out.println(NOT_FOUND);
        }

    }

    @Override
    public void transactionListByBankonat(User user) {
        bankomatList.stream()
                .forEach(bankomat -> {
                    System.out.println("id:" + bankomat.getId() + ", name : " + bankomat.getName());
                });
        System.out.println("Enter bankomat id");
        int bankomatId = scannerInt.nextInt();
        Bankomat bankomat = existAction.findBankomat(bankomatId);
        if (bankomat != null) {
            switch (user.getRole()) {
                case ADMIN: {
                    transactionHistoryList.stream()
                            .filter(transactionHistory -> transactionHistory.getBankomat().equals(bankomat))
                            .forEach(transactionHistory ->
                            {
                                System.out.println("Type     : " + transactionHistory.getType());
                                System.out.println("Card     : " + transactionHistory.getBankCard().getCardNumber());
                                System.out.println("Bankomat : " + transactionHistory.getBankomat().getName());
                                System.out.println("Amount   : " + transactionHistory.getAmount());
                                System.out.println("Where    : " + transactionHistory.getWhere());
                                System.out.println("Fee      : " + transactionHistory.getBankomat().getCommissionFee());
                                System.out.println("Total    : " +
                                        (transactionHistory.getAmount() +
                                                (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100)));
                                System.out.println("--------------------------------------------");


                            });
                    break;
                }
                case USER: {
                    transactionHistoryList.stream()
                            .filter(transactionHistory -> transactionHistory.getBankCard().getUser().equals(user)
                                    && transactionHistory.getBankomat().equals(bankomat))
                            .forEach(transactionHistory ->
                            {
                                System.out.println("Type     : " + transactionHistory.getType());
                                System.out.println("Card     : " + transactionHistory.getBankCard().getCardNumber());
                                System.out.println("Bankomat : " + transactionHistory.getBankomat().getName());
                                System.out.println("Amount   : " + transactionHistory.getAmount());
                                System.out.println("Where    : " + transactionHistory.getWhere());
                                System.out.println("Fee      : " + transactionHistory.getBankomat().getCommissionFee());
                                System.out.println("Total    : " +
                                        (transactionHistory.getAmount() +
                                                (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100)));
                                System.out.println("--------------------------------------------");

                            });
                    break;
                }

            }
        } else {
            System.out.println(NOT_FOUND);
        }
    }

    @Override
    public void typeHistory(User user) {
        System.out.println("1=>CARD TO CARD 2=>WITHDRAW 3=>FILL 4=>PAY WHERE");
        TransactionHistory transactionHistory = new TransactionHistory();
        int option = scannerInt.nextInt();
        switch (option) {
            case 1:
                transactionHistory.setType(TransactionType.CART_TO_CARD);
                typeHistoryTr(transactionHistory.getType(), user);
                break;
            case 2:
                transactionHistory.setType(TransactionType.WITHDRAW_MONEY);
                typeHistoryTr(transactionHistory.getType(), user);
                break;
            case 3:
                transactionHistory.setType(TransactionType.FILL_BALANCE);
                typeHistoryTr(transactionHistory.getType(), user);
                break;
            case 4:
                transactionHistory.setType(TransactionType.PAY_INDUSTRY);
                typeHistoryTr(transactionHistory.getType(), user);
                break;
            default:
                System.out.println(WRONG_OPTION);
                break;
        }
    }

    public void typeHistoryTr(TransactionType transactionType, User user) {
        switch (user.getRole()) {
            case ADMIN: {
                double total = 0;
                for (TransactionHistory transactionHistory : transactionHistoryList) {
                    if (transactionHistory.getType().equals(transactionType)) {
                        System.out.println("Type     : " + transactionHistory.getType());
                        System.out.println("Card     : " + transactionHistory.getBankCard().getCardNumber());
                        System.out.println("Bankomat : " + transactionHistory.getBankomat().getName());
                        System.out.println("Amount   : " + transactionHistory.getAmount());
                        System.out.println("Where    : " + transactionHistory.getWhere());
                        System.out.println("Fee      : " + transactionHistory.getBankomat().getCommissionFee());
                        System.out.println("Total    : " +
                                (transactionHistory.getAmount() +
                                        (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100)));
                        total += (transactionHistory.getAmount() +
                                (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100));
                        System.out.println("--------------------------------------------");
                    }

                }
                System.out.println("Total : " + total);
                System.out.println("--------------------------------------------");
                break;
            }
            case USER: {
                double total = 0;
                for (TransactionHistory transactionHistory : transactionHistoryList) {
                    if (transactionHistory.getType().equals(transactionType) && transactionHistory.getBankCard().getUser().equals(user)) {
                        System.out.println("Type     : " + transactionHistory.getType());
                        System.out.println("Card     : " + transactionHistory.getBankCard().getCardNumber());
                        System.out.println("Bankomat : " + transactionHistory.getBankomat().getName());
                        System.out.println("Amount   : " + transactionHistory.getAmount());
                        System.out.println("Where    : " + transactionHistory.getWhere());
                        System.out.println("Fee      : " + transactionHistory.getBankomat().getCommissionFee());
                        System.out.println("Total    : " +
                                (transactionHistory.getAmount() +
                                        (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100)));
                        total += (transactionHistory.getAmount() +
                                (transactionHistory.getAmount() * transactionHistory.getBankomat().getCommissionFee() / 100));
                        System.out.println("--------------------------------------------");
                    }


                }
                System.out.println("Total : " + total);
                System.out.println("--------------------------------------------");
                break;
            }
        }
    }
}

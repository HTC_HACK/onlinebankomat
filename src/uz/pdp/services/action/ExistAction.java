package uz.pdp.services.action;

import uz.pdp.model.BankCard;
import uz.pdp.model.Bankomat;
import uz.pdp.model.User;

import static uz.pdp.DB.*;

public class ExistAction {

    public boolean checkUser(String username) {
        for (User user : userList) if (user.getUsername().equals(username)) return true;
        return false;
    }

    public User checkUserLogin(String username, String password) {
        for (User user : userList)
            if (user.getUsername().equals(username) && user.getPassword().equals(password))
                return user;
        return null;
    }

    public boolean checkBankomat(String name) {
        for (Bankomat bankomat : bankomatList) {
            if (bankomat.getName().equals(name))
                return true;
        }
        return false;
    }

    public Bankomat findBankomat(int id) {
        return bankomatList.stream()
                .filter(bankomat -> bankomat.getId() == id)
                .findFirst().orElse(null);
    }

    public boolean existCard(Long card) {
        for (BankCard bankCard : cardList) {
            if (bankCard.getCardNumber() == card)
                return true;
        }

        return false;
    }

    public BankCard findBankCard(int id) {
        for (BankCard card : cardList) {
            if (card.getId() == id)
                return card;
        }
        return null;
    }

    public BankCard findBankCardNum(long id) {
        for (BankCard card : cardList) {
            if (card.getCardNumber() == id)
                return card;
        }
        return null;
    }

    public BankCard findBankCardNumPin(Long num, int pin) {
        return cardList.stream()
                .filter(card -> card.getCardNumber().equals(num) && card.getPinCode() == pin)
                .findFirst().orElse(null);
    }
}

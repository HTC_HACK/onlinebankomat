package uz.pdp.services;

import com.sun.org.apache.bcel.internal.generic.BREAKPOINT;
import uz.pdp.model.BankCard;
import uz.pdp.model.User;
import uz.pdp.model.enums.CardStatus;
import uz.pdp.model.enums.UserRole;
import uz.pdp.services.action.ExistAction;
import uz.pdp.services.builder.UserBuilder;
import uz.pdp.services.interfaceSer.UserService;

import static uz.pdp.DB.*;
import static uz.pdp.model.messages.MessageHelper.*;

public class UserServiceImpl implements UserService {

    static ExistAction existAction = new ExistAction();
    static UserMenu userMenu = new UserMenu();

    @Override
    public void login() {
        System.out.println("1=>Login BankCard. 2=>Login Username");
        int option = scannerInt.nextInt();

        switch (option) {
            case 1:
                int count = 0;
                System.out.println("Enter cardNum");
                long card = scannerLong.nextLong();
                BankCard bankCard = existAction.findBankCardNum(card);
                if (bankCard != null) {
                    boolean active = true;
                    while (active) {
                        if (count == 3) {
                            bankCard.setStatus(CardStatus.BLOCK);
                            System.out.println("Card Blocked");
                            return;
                        }
                        System.out.println("Enter pin or back=>0");
                        int pin = scannerInt.nextInt();
                        if (pin == 0) return;
                        BankCard card1 = existAction.findBankCardNumPin(card, pin);
                        if (card1 != null) {
                            userMenu.userMenu(card1.getUser());
                        } else {
                            count++;
                        }
                    }
                }
                break;
            case 2: {
                System.out.println("Enter username");
                String username = scannerStr.nextLine();
                System.out.println("Enter password");
                String password = scannerStr.nextLine();
                User user = existAction.checkUserLogin(username, password);
                if (user != null) {
                    System.out.println("Welcome " + user.getUsername());
                    userMenu.userMenu(user);

                } else {
                    System.out.println(NOT_FOUND);
                }
                break;
            }
        }

    }

    @Override
    public void register() {
        System.out.println("Enter username");
        String username = scannerStr.nextLine();
        System.out.println("Enter password");
        String password = scannerStr.nextLine();
        System.out.println("Confirm password");
        String confirmPassword = scannerStr.nextLine();

        if (!existAction.checkUser(username) && password.equals(confirmPassword)) {

            UserBuilder userBuilder = User::new;
            User user = userBuilder.create((int) (Math.random() * 10000), username, password, UserRole.USER);
            userList.add(user);
            System.out.println(CREATED);
        } else {
            System.out.println(USER_EXIST);
        }

    }

    @Override
    public void users() {
        userList.stream()
                .forEach(user ->
                {
                    System.out.println("id : " + user.getId() + ", username : " + user.getUsername() + ", role:" + user.getRole());
                });
    }

}

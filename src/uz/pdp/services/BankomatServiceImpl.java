package uz.pdp.services;

import uz.pdp.model.Bankomat;
import uz.pdp.services.action.ExistAction;
import uz.pdp.services.builder.BankomatBuilder;
import uz.pdp.services.interfaceSer.BankomatService;

import static uz.pdp.DB.*;
import static uz.pdp.model.messages.MessageHelper.*;

public class BankomatServiceImpl implements BankomatService {

    static ExistAction existAction = new ExistAction();

    @Override
    public void createBankomat() {
        System.out.println("Enter name");
        String name = scannerStr.nextLine();
        System.out.println("Enter commission");
        double fee = scannerDou.nextDouble();
        if (!existAction.checkBankomat(name)) {
            BankomatBuilder bankomatBuilder = Bankomat::new;
            Bankomat bankomat = bankomatBuilder.create((int) (Math.random() * 10000), name, fee);
            bankomatList.add(bankomat);
            System.out.println(CREATED);
        } else {
            System.out.println(EXIST);
        }
    }

    @Override
    public void bankomatList() {
        bankomatList.stream()
                .forEach(bankomat -> {
                    System.out.println("ID:" + bankomat.getId() + ", name : " + bankomat.getName() + ", fee : " + bankomat.getCommissionFee());
                });
    }

    @Override
    public void updateBankomat() {
        bankomatList();
        System.out.println("Enter id");
        int id = scannerInt.nextInt();
        Bankomat bankomat = existAction.findBankomat(id);
        if (bankomat != null) {
            System.out.println("Enter commissionFee");
            double fee = scannerDou.nextDouble();
            if (fee > 0) {
                bankomat.setCommissionFee(fee);
                System.out.println(SUCCESS);
            } else {
                System.out.println(FAIL);
            }
        } else {
            System.out.println(NOT_FOUND);
        }
    }

    @Override
    public void deleteBankomat() {
        bankomatList();
        System.out.println("Enter id");
        int id = scannerInt.nextInt();
        Bankomat bankomat = existAction.findBankomat(id);
        if (bankomat != null) {
            bankomatList.remove(bankomat);
            System.out.println(SUCCESS);
        } else {
            System.out.println(NOT_FOUND);
        }
    }
}

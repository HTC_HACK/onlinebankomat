package uz.pdp.services;

import uz.pdp.model.BankCard;
import uz.pdp.model.Bankomat;
import uz.pdp.model.TransactionHistory;
import uz.pdp.model.User;
import uz.pdp.model.enums.TransactionType;
import uz.pdp.services.action.ExistAction;
import uz.pdp.services.interfaceSer.BankCardService;

import static uz.pdp.DB.*;
import static uz.pdp.model.enums.CardStatus.BLOCK;
import static uz.pdp.model.enums.CardStatus.UNBLOCK;
import static uz.pdp.model.messages.MessageHelper.*;

public class BankCardServiceImpl implements BankCardService {

    static ExistAction existAction = new ExistAction();
    static BankomatServiceImpl bankomatService = new BankomatServiceImpl();

    @Override
    public void createCard(User user) {
        System.out.println("Enter card number");
        Long number = scannerLong.nextLong();
        System.out.println("Enter pin");
        int pin = scannerInt.nextInt();

        if (!existAction.existCard(number)) {
            BankCard card = new BankCard((int) (Math.random() * 10000), number, pin, UNBLOCK, user);
            cardList.add(card);
            System.out.println(CREATED);
        } else {
            System.out.println(EXIST);
        }

    }

    @Override
    public void blockedcardList(User user) {
        cardList.stream()
                .filter(bankCard -> bankCard.getStatus().equals(BLOCK))
                .forEach(card -> {
                    System.out.println("Card : " + card.getCardNumber() + ", id: " + card.getId() + ", status : " + card.getStatus());
                });

    }

    @Override
    public void payIndustry(User user) {
        bankomatService.bankomatList();

        System.out.println("Enter bankomat id");
        int idBAN = scannerInt.nextInt();
        Bankomat bankomat = existAction.findBankomat(idBAN);
        if (bankomat != null) {
            cardList(user);
            System.out.println("Enter you card id:");
            int id = scannerInt.nextInt();
            BankCard card = existAction.findBankCard(id);
            if (card != null && card.getStatus().equals(UNBLOCK)) {

                System.out.println("Enter place you want to pay");
                String where = scannerStr.nextLine();

                System.out.println("Enter amount");
                double amount = scannerDou.nextDouble();
                double total = (bankomat.getCommissionFee() * amount / 100) + amount;

                if (card.getBalance() >= total) {
                    card.setBalance(card.getBalance() - total);
                    TransactionHistory transactionHistory = new TransactionHistory(
                            (int) (Math.random() * 10000),
                            bankomat,
                            card,
                            where,
                            TransactionType.PAY_INDUSTRY,
                            amount
                    );
                    transactionHistoryList.add(transactionHistory);
                    System.out.println(SUCCESS);
                } else {
                    System.out.println(FAIL);
                }

            } else {
                System.out.println(NOT_FOUND);
            }
        }
    }

    @Override
    public void cardList(User user) {
        switch (user.getRole()) {
            case ADMIN:
                cardList.stream()
                        .forEach(card -> {
                            System.out.println("Card : " + card.getCardNumber() + ", id: " + card.getId() + ", status : " + card.getStatus());
                        });
                break;
            case USER: {
                cardList.stream()
                        .filter(card -> card.getUser().equals(user))
                        .forEach(card -> {
                            System.out.println("Card : " + card.getCardNumber() + ", id: " + card.getId() + ", balance : " + card.getBalance() + ", status : " + card.getStatus());
                        });
                break;
            }
        }

    }

    @Override
    public void activateCard(User user) {
        blockedcardList(user);
        System.out.println("Enter id");
        int id = scannerInt.nextInt();
        BankCard card = existAction.findBankCard(id);
        if (card != null && card.getStatus().equals(BLOCK)) {
            card.setStatus(UNBLOCK);
            System.out.println(SUCCESS);
        } else {
            System.out.println(ACTIVE);
        }

    }

    @Override
    public void fillBalance(User user) {
        bankomatService.bankomatList();

        System.out.println("Enter bankomat id");
        int bankomatId = scannerInt.nextInt();
        Bankomat bankomat = existAction.findBankomat(bankomatId);
        if (bankomat != null) {
            cardList(user);
            System.out.println("Enter id");
            int id = scannerInt.nextInt();
            cardList(user);
            BankCard card = existAction.findBankCard(id);
            if (card != null && card.getStatus().equals(UNBLOCK)) {
                System.out.println("Enter amount");
                double amount = scannerDou.nextDouble();
                if (amount > 0) {
                    card.setBalance(card.getBalance() + amount);
                    TransactionHistory transaction = new TransactionHistory(
                            (int) (Math.random() * 10000), bankomat
                            , card,
                            card.getUser().getUsername(),
                            TransactionType.FILL_BALANCE, amount);
                    transactionHistoryList.add(transaction);

                    System.out.println(SUCCESS);
                } else {
                    System.out.println(FAIL);
                }
            } else {
                System.out.println(FAIL);
            }
        } else {
            System.out.println(NOT_FOUND);
        }
    }

    @Override
    public void changePin(User user) {

        cardList(user);
        System.out.println("Enter card id");
        int id = scannerInt.nextInt();
        BankCard bankCard = existAction.findBankCard(id);
        if (bankCard != null && bankCard.getStatus().equals(UNBLOCK)) {
            System.out.println("Enter old pin");
            int oldPin = scannerInt.nextInt();
            if (bankCard.getPinCode() == oldPin) {
                System.out.println("Enter new pin");
                int newPin = scannerInt.nextInt();
                System.out.println("Enter Confirm pin");
                int conPin = scannerInt.nextInt();
                if (newPin == conPin) {
                    bankCard.setPinCode(newPin);
                    System.out.println(SUCCESS);
                } else {
                    System.out.println(CONFIMATION_FAIL);
                }
            } else {
                System.out.println(FAIL);
            }
        } else {
            System.out.println(NOT_FOUND);
        }

    }

    @Override
    public void cardToCard(User user) {
        bankomatService.bankomatList();

        System.out.println("Enter bankomat id");
        int idBAN = scannerInt.nextInt();
        Bankomat bankomat = existAction.findBankomat(idBAN);
        if (bankomat != null) {
            cardList(user);
            System.out.println("Enter you card id:");
            int id = scannerInt.nextInt();
            BankCard card = existAction.findBankCard(id);
            if (card != null && card.getStatus().equals(UNBLOCK)) {

                System.out.println("Enter card number you want to transfer");
                long cardNUmber = scannerLong.nextLong();

                System.out.println("Enter amount");
                double amount = scannerDou.nextDouble();
                double total = (bankomat.getCommissionFee() * amount / 100) + amount;

                if (card.getBalance() >= total) {
                    card.setBalance(card.getBalance() - total);
                    TransactionHistory transactionHistory = new TransactionHistory(
                            (int) (Math.random() * 10000),
                            bankomat,
                            card,
                            "" + cardNUmber,
                            TransactionType.CART_TO_CARD,
                            amount
                    );
                    transactionHistoryList.add(transactionHistory);
                    System.out.println(SUCCESS);
                } else {
                    System.out.println(FAIL);
                }

            } else {
                System.out.println(NOT_FOUND);
            }
        } else {
            System.out.println(NOT_FOUND);
        }


    }

    @Override
    public void withDraw(BankCard card) {
        bankomatService.bankomatList();

        System.out.println("Enter bankomat id");
        int id = scannerInt.nextInt();
        Bankomat bankomat = existAction.findBankomat(id);
        if (bankomat != null) {
            System.out.println("Enter amount");
            double amount = scannerDou.nextDouble();
            double total = amount + amount * bankomat.getCommissionFee() / 100;
            if (card.getBalance() >= total) {
                card.setBalance(card.getBalance() - total);
                TransactionHistory transactionHistory = new TransactionHistory(
                        (int) (Math.random() * 10000),
                        bankomat,
                        card,
                        "out",
                        TransactionType.WITHDRAW_MONEY,
                        amount
                );
                transactionHistoryList.add(transactionHistory);
                System.out.println(SUCCESS);
            } else {
                System.out.println(FAIL);
            }
        } else {
            System.out.println(NOT_FOUND);
        }
    }

    public void chooseTransactionType(User user) {

        while (true) {
            System.out.println("1=>Withdraw 2=>FillBalance 3=>CardToCard. 4=>PaySomeWhere 5=>Back");
            int option = scannerInt.nextInt();
            switch (option) {
                case 1: {
                    cardList(user);
                    System.out.println("Enter card id");
                    int id = scannerInt.nextInt();
                    BankCard card = existAction.findBankCard(id);
                    if (card != null && card.getStatus().equals(UNBLOCK)) {
                        withDraw(card);
                    } else {
                        System.out.println(FAIL);
                    }
                    break;
                }
                case 2: {
                    fillBalance(user);
                    break;
                }
                case 3:
                    cardToCard(user);
                    break;
                case 4:
                    payIndustry(user);
                    break;
                case 5:
                    return;
            }
        }
    }

    public void seeBalance(User user) {
        cardList(user);
    }
}

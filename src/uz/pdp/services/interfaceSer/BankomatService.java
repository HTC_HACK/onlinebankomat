package uz.pdp.services.interfaceSer;

import uz.pdp.model.Bankomat;

public interface BankomatService {

    void createBankomat();

    void bankomatList();

    void updateBankomat();

    void deleteBankomat();


}

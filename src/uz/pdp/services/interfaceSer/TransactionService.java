package uz.pdp.services.interfaceSer;

import uz.pdp.model.User;

public interface TransactionService {

    void transactionList(User user);

    void transactionListByCard(User user);

    void transactionListByBankonat(User user);

    void typeHistory(User user);


}

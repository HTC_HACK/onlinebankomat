package uz.pdp.services.interfaceSer;

import uz.pdp.model.BankCard;
import uz.pdp.model.Bankomat;
import uz.pdp.model.User;

public interface BankCardService {

    void createCard(User user);

    void blockedcardList(User user);

    void payIndustry(User user);

    void cardList(User user);

    void activateCard(User user);

    void fillBalance(User user);

    void changePin(User user);

    void cardToCard(User user);

    void withDraw(BankCard card);
}

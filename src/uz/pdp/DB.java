package uz.pdp;

import uz.pdp.model.BankCard;
import uz.pdp.model.Bankomat;
import uz.pdp.model.TransactionHistory;
import uz.pdp.model.User;
import uz.pdp.model.enums.UserRole;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class DB {

    public static Scanner scannerStr = new Scanner(System.in);
    public static Scanner scannerInt = new Scanner(System.in);
    public static Scanner scannerDou = new Scanner(System.in);
    public static Scanner scannerLong = new Scanner(System.in);

    public static List<Bankomat> bankomatList = new ArrayList<>();
    public static List<User> userList = new ArrayList<>(
            Arrays.asList(
                    new User(1, "admin", "12345", UserRole.ADMIN)
            )
    );
    public static List<BankCard> cardList = new ArrayList<>();
    public static List<TransactionHistory> transactionHistoryList = new ArrayList<>();


}
